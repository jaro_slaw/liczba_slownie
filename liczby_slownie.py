__author__ = 'supervisor'


def convert_to_string (number):
    number = int(number)
    max_range = 2

    if abs(number) > max_range:
        return "Przykro mi, niestety aktualnie wypisuje liczby z przedzialu [-" + str(max_range) + "," + str(max_range) + "]"


    if number < 0:
        string = "minus "
        number = abs(number)
    else:
        string = ""

    if number == 0:
        string = "zero"
    elif number == 1:
        string += "jeden"
    elif number == 2:
        string += "dwa"
    else:
        string = "***NIESTETY, wystapil nieoczekiwany blad***"

    return string

print "Wypisywacz liczb slownie!"

number = raw_input("Prosze wpisac numer ")

print "Wybrano numer: " +  number + ", czyli slownie: " + convert_to_string(number)